# analysis-for-versioning-terminologies

This repository contains the script for comparing codes (terms) across different versions of the same terminology. 

The script requires ATC, CHOP and ICD-10-GM in RDF Turtle serializtion where each Turtle file corresponds to codes from a single year of a terminology.

> Some of the RDF Turtle files of these terminologies can be requested to dcc@sib.swiss.


## Installing dependencies

```
pip install -r requirements.txt
```


## Running the comparison

The script usage:

```
Usage: compare_codes_across_versions.py [OPTIONS] TERMINOLOGY
                                        TERMINOLOGY_FOLDER

Arguments:
  TERMINOLOGY         [required]
  TERMINOLOGY_FOLDER  [required]

Options:
  --help  Show this message and exit.
```


To run the script for ATC,

```
python compare_codes_across_versions.py ATC data/ATC
```

To run the script for CHOP,

```
python compare_codes_across_versions.py CHOP data/CHOP
```

To run the script for ICD-10-GM,

```
python compare_codes_across_versions.py ICD-10-GM data/ICD-10-GM
```

