import os
from pathlib import Path
from typing import List, Dict
import typer
import jellyfish
import pandas as pd
import matplotlib.pyplot as plt
from rdflib import Graph
from rdflib import RDFS


def preprocess_text(text: str) -> str:
    """
    Function to preprocess the input text.

    Args:
        text: The input text

    Returns:
        Preprocessed text

    """
    text = text.lower()
    text = text.strip()
    if '<o>' in text and '</o>' in text:
        text = text.replace('<o>', '')
        text = text.replace('</o>', '')
    if '<n>' in text and '</n>' in text:
        text = text.replace('<n>', '')
        text = text.replace('</n>', '')
    if '[' in text and ']' in text:
        text = text.replace('[', '(')
        text = text.replace(']', ')')
    return text


def compare_graphs(current_year: str, current_year_filename: str, prior_year: str, prior_year_filename: str, lang: str, preprocess: bool = False, cutoff: float = 1.0) -> int:
    """
    Compare codes from RDF Turtle files where one is the RDF Turtle for the current year
    and the other is the RDF Turtle for the prior year.
    
    Args:
        current_year: Current year
        current_year_filename: Filename corresponding to codes from current year
        prior_year: Prior year
        prior_year_filename: Filename corresponding to the codes from prior year
        lang: The language for labels
        preprocess: Whether or not to preprocess the code labels
        cutoff: Float value indicating the cutoff for the similarity score

    Returns:
        The number of codes that are considered as different based on cutoff

    """
    current_graph = Graph()
    current_graph.parse(current_year_filename)
    prior_graph = Graph()
    prior_graph.parse(prior_year_filename)
    count = 0
    for code in current_graph.subjects(predicate=RDFS.label):
        if lang:
            current_labels = [str(x) for x in current_graph.objects(code, RDFS.label) if x.language == lang]
            prior_labels = [str(x) for x in prior_graph.objects(code, RDFS.label) if x.language == lang]
        else:
            current_labels = [str(x) for x in current_graph.objects(code, RDFS.label)]
            prior_labels = [str(x) for x in prior_graph.objects(code, RDFS.label)]
        
        if current_labels and prior_labels:
            if len(current_labels) > 1 or len(prior_labels) > 1:
                print(f"[WARNING] {code} has more than one label")
            if preprocess:
                current_label = preprocess_text(current_labels[0])
                prior_label = preprocess_text(prior_labels[0])
            else:
                current_label = current_labels[0]
                prior_label = prior_labels[0]

            sim_score = jellyfish.jaro_similarity(current_label, prior_label)
            print(f"[{current_year} vs {prior_year}][{code}] Comparing '{current_label}' with '{prior_label}' yielded a jaro similarity of {sim_score}")
            if sim_score < cutoff:    
                count += 1
    return count


def plot_multiple(df1: pd.DataFrame, df2: pd.DataFrame, df3: pd.DataFrame, filename: str = None) -> None:
    """
    Plot 3 dataframes together into a single plot and write the plot
    to a file (if filename is provided).

    Args:
        df1: Data Frame 1
        df2: Data Frame 2
        df3: Data Frame 3
        filename: The output filename

    """
    fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))
    max_y_value = max(df1.max().max(), df2.max().max(), df3.max().max()) + 20
    min_y_value = 0

    df1.plot(kind='bar', ax=axes[0])
    axes[0].set_title('A')
    axes[0].set_xlabel('Year')
    axes[0].set_ylabel('Count')
    axes[0].set_ylim([min_y_value, max_y_value])

    df2.plot(kind='bar', ax=axes[1], color='green')
    axes[1].set_title('B')
    axes[1].set_xlabel('Year')
    axes[1].set_ylabel('Count')
    axes[1].set_ylim([min_y_value, max_y_value])

    df3.plot(kind='bar', ax=axes[2], color='red')
    axes[2].set_title('C')
    axes[2].set_xlabel('Year')
    axes[2].set_ylabel('Count')
    axes[2].set_ylim([min_y_value, max_y_value])

    plt.tight_layout()

    if filename:
        plt.savefig(filename, format='pdf', dpi=300)
    else:
        plt.show()



LANG_MAP = {
    'ATC': 'en',
    'CHOP': 'de',
    'ICD-10-GM': 'de'
}


def main(terminology: str, terminology_folder: Path):
    stats1 = {}
    stats2 = {}
    stats3 = {}
    stats1[terminology] = {}
    stats2[terminology] = {}
    stats3[terminology] = {}
    filenames = [file for file in os.listdir(terminology_folder)]
    sorted_filenames = sorted(filenames)
    for i in range(len(sorted_filenames)):
        for j in range(i+1, len(sorted_filenames)):
            source_version_filename = sorted_filenames[i]
            target_version_filename = sorted_filenames[j]
            source_year = int(source_version_filename.split("_")[2].split("-")[0])
            target_year = int(target_version_filename.split("_")[2].split("-")[0])
            source_version_filepath = str(terminology_folder.absolute()) + os.path.sep + source_version_filename
            target_version_filepath = str(terminology_folder.absolute()) + os.path.sep + target_version_filename
            if (target_year - 1) == source_year:
                    stats1[terminology][f'{target_year} vs {source_year}'] = compare_graphs(target_year, target_version_filepath, source_year, source_version_filepath, LANG_MAP[terminology], preprocess=False, cutoff=1.0)
                    stats2[terminology][f'{target_year} vs {source_year}'] = compare_graphs(target_year, target_version_filepath, source_year, source_version_filepath, LANG_MAP[terminology], preprocess=True, cutoff=1.0)
                    stats3[terminology][f'{target_year} vs {source_year}'] = compare_graphs(target_year, target_version_filepath, source_year, source_version_filepath, LANG_MAP[terminology], preprocess=True, cutoff=0.85)

    df1 = pd.DataFrame([(outer_key, inner_key, inner_val) for outer_key, inner_dict in stats1.items() for inner_key, inner_val in inner_dict.items()], columns=['Category', 'Year', 'Value'])
    df1_pivoted = df1.pivot(index='Year', columns='Category', values='Value').fillna(0)
    df2 = pd.DataFrame([(outer_key, inner_key, inner_val) for outer_key, inner_dict in stats2.items() for inner_key, inner_val in inner_dict.items()], columns=['Category', 'Year', 'Value'])
    df2_pivoted = df2.pivot(index='Year', columns='Category', values='Value').fillna(0)
    df3 = pd.DataFrame([(outer_key, inner_key, inner_val) for outer_key, inner_dict in stats3.items() for inner_key, inner_val in inner_dict.items()], columns=['Category', 'Year', 'Value'])
    df3_pivoted = df3.pivot(index='Year', columns='Category', values='Value').fillna(0)
    plot_multiple(df1_pivoted, df2_pivoted, df3_pivoted, f"{terminology}_comparison.pdf")


if __name__ == "__main__":
    typer.run(main)